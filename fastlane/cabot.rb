require "uri"
require "net/http"
require "json"

# notify to workchat
def cabot_notify(workchat_id, release_notes)
    cabot_token = ENV["CABOT_TOKEN"]
    url = URI("https://graph.facebook.com/v2.6/me/messages?access_token=#{cabot_token}")
    https = Net::HTTP.new(url.host, url.port);
    https.use_ssl = true
  
    recipient = { 'thread_key' => workchat_id }
    message = { 'text' => release_notes }
    hash = { 'recipient' => recipient, 'message' => message }
    body = hash.to_json
    
    request = Net::HTTP::Post.new(url)
    request["Content-Type"] = ["application/json"]
    request.body = body
  
    response = https.request(request)
end